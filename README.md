## What is this?

This is a nixpkgs overlay in flake format, automatically updated daily. It gives the latest versions of the various jetbrains IDEs.

As most of the code in this repo is copied from `<nixos/nixpkgs>/pkgs/applications/editors/jetbrains`, it has the same license (MIT).
However, many jetbrains packages are unfree and, for the sake of convenience, the `pkgs` output has `allowUnfree` set to true.
The overlay should refuse to evaluate if `allowUnfree` is true.

Be aware, this is a thing I'm making for personal use. As such, there is basically no testing other than making it work on my machine.

# Features

- Newest IDEs (checked and updated daily)
- Plugins (also updated daily, see [#223593](https://github.com/NixOS/nixpkgs/pull/223593) for info)
- Typically, a newer `jetbrains.jdk` than nixpkgs (if a new stable is out but this hasn't been updated, make an issue)

## Limitations
 - only flakes are supported
 - nixpkgs-unstable is required
   - you might be able to work around this by making `libxcrypt-legacy` point to `libxcrypt`, its only needed for clion
 - only x86_64-linux has any kind of testing, expect all others to require work

# How do I use it?

## Setup

Add to inputs: `jetbrains-updater.url = "gitlab:genericnerdyusername/jetbrains-updater";`

then 
 ```nix
{
  nixpkgs.overlays = [ jetbrains-updater.overlay ];
}
```

OR

 ```nix
{
  nixosConfigurations.nixos = {
    system = "x86_64-linux";
    modules = [
      jetbrains-updater.nixosModules.jetbrains-updater
    ];
  };
}
```

## Basic usage

If all you want is newer versions, the only thing that's needed is
```nix
{ pkgs }:
{
  environment.systemPackages = with pkgs.jetbrains; [
    clion
    datagrip
    gateway
    goland
    idea-community
    idea-ultimate
    mps
    phpstorm
    pycharm-community
    pycharm-professional
    rider
    ruby-mine
    rust-rover
    webstorm
  ];
}
```
(remove unneeded entries as appropriate)


## Plugins

This repo also supports declaratively adding plugins, including patching some plugins that would otherwise be broken
```nix
{ pkgs }:
{
  environment.systemPackages = with pkgs.jetbrains; [
    (addPlugins clion [
      "nixidea" # You can use names
      "164" # or ids

      # Append the channel name to the plugin name or id to get beta versions
      "rust-beta"
      
      # If a plugin is a derivation instead of a string, it will be added directly
      # Note that this cant chck for (in)compatibility
      ./my-custom-plugin.jar
    ])
  ];
}
```
For a list of available plugins, see `jetbrains/plugins/plugins.json`

Things may break if the plugins installed declaratively are installed "over" others
(ie uninstall plugins before adding them declaratively)

### How to add plugins not already in the repo:
1. fork
2. `cd jetbrains-updater/jetbrains/plugins`
3. `./update_plugins.py add [id]`
4. if the plugin requires special treatment (for example, patching a native binary), modify `specialPlugins.nix`
