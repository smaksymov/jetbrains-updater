{
  description = "Automatic updates for jetbrains products";

  outputs = { nixpkgs, ... }:
    let
      overlay = (final: prev: {
        jetbrains = final.callPackage ./jetbrains { jdk = final.jetbrains.jdk; }
          // {
          jdk = final.callPackage ./jdk { };
          jdk-no-jcef = final.callPackage ./jdk { withJcef = false; };
          jcef = final.callPackage ./jdk/jcef.nix { };
        };
      });
      pkgs = import nixpkgs { system = "x86_64-linux"; overlays = [ overlay ]; config.allowUnfree = true; };
    in
    rec {
      inherit overlay;
      overlays.default = overlay;
      overlays.jetbrains = overlay;
      nixosModules.jetbrains-updater.nixpkgs.overlays = [ overlay ];
      checks.x86_64-linux = pkgs.jetbrains.plugins.tests;
      packages.x86_64-linux = { inherit (pkgs) jetbrains; };
    };

}
